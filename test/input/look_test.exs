defmodule Test.Input.LookTest do
  use Test.Support.HyPegTest

  import Input, only: [next_char: 1]

  describe "next_char" do
    test "present" do
      {graph, rest} = next_char(mk_input("Hello"))
      assert graph == "H"
      assert rest == mk_input("ello", 1, true)
    end

    test "a serie of advancements" do
      input = mk_input(~W[a b])
      {g1, inp1} = next_char(input)
      assert g1 == "a"
      {g2, inp2} = next_char(inp1)
      assert g2 == :eol
      {g3, inp3} = next_char(inp2)
      assert g3 == "b"
      {g4, inp4} = next_char(inp3)
      assert g4 == :eol
      {g5, _inp5} = next_char(inp4)
      assert g5 == :eof
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
