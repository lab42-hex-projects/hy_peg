defmodule Test.Input.CrationTest do
  use Test.Support.HyPegTest

  doctest HyPeg.Input, import: true

  import Input, only: [new: 1, new: 2, new: 3]

  describe "Input.new from a binary" do
    test "one line as string, defaults for col and lnb" do
      assert new("Hello") == struct(Input, col: 1, lines: ["Hello"], lnb: 1)
    end

    test "one line as string, explicit lnb" do
      assert new("Hello", 42) == struct(Input, col: 1, lines: ["Hello"], lnb: 42)
    end

    test "one line, explicit lnb and col" do
      assert new("Hello", 43, 73) == struct(Input, lines: ["Hello"], col: 73, lnb: 43)
      
    end
  end

  describe "Input.new from a binaries" do
    test "one line, defaults for col and lnb" do
      input = ["One line"]
      expected = struct(Input, col: 1, lines: ["One line"], lnb: 1)
      assert new(input) == expected
    end

    test "one line, explicit, lnb" do
      input = ["One line"]
      expected = struct(Input, col: 1, lines: ["One line"], lnb: 2)
      assert new(input, 2) == expected
    end

    test "some lines and defaults" do
      input = ["First line", "", "Third line"]
      expected = struct(Input, col: 1, lines: input, lnb: 1)
      assert new(input) == expected
    end

    test "some lines, explict col and lnb" do
      input = ["First line", "", "Third line"]
      expected = struct(Input, col: 22, lines: input, lnb: 11)
      assert new(input, 11, 22) == expected
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
