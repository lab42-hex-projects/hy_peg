defmodule Test.Support.InputHelpers do
  @moduledoc false
  
  def mk_input(lines, offset \\ 1, expand \\ false) do
    if expand do
      HyPeg.Input.new(lines, offset) |> HyPeg.Input.expand
    else
      HyPeg.Input.new(lines, offset)
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
