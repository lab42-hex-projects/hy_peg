defmodule Test.Support.HyPegTest do
  @moduledoc false

  defmacro __using__(_opts) do
    quote do
      use ExUnit.Case
      import Test.Support.InputHelpers

      alias HyPeg.{Input}
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
