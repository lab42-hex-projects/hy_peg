defmodule HyPeg.Success do
  use HyPeg.Types

  alias HyPeg.{Cache, Input}

  @moduledoc ~S"""
  A success result
  """
  defstruct ast: nil, cache: %Cache{}, rest: %Input{}

  @type t :: %__MODULE__{ast: any(), cache: Cache.t(), rest: Input.t()}

  @spec succeed(ast_t(), Input.t(), Cache.t()) :: t
  def succeed(ast, rest, cache) do
    %__MODULE__{ast: ast, cache: cache, rest: rest}
  end
end
# SPDX-License-Identifier: Apache-2.0
