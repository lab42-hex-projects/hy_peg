defmodule HyPeg.Input do
  use HyPeg.Types

  @moduledoc ~S"""
  A struct representing the hybrid input and operations on it
  """

  defstruct col: 1, context: {}, last_line: nil, lines: [], lnb: 1

  @type position_t :: {pos_integer(), pos_integer()}
  @type token_t :: binary() | :eol | :eof
  @type t :: %__MODULE__{
          col: pos_integer(),
          context: map(),
          last_line: binary?(),
          lines: binaries(),
          lnb: pos_integer()
        }

  @doc ~S"""
  A convenience method to create this struct

      iex(1)> new("a")
      %Input{lines: ["a"], lnb: 1}

      iex(2)> new(~W[alpha beta], 42)
      %Input{lines: ~W[alpha beta], lnb: 42}

      iex(3)> new("just one line", 43, 74)
      %Input{lines: ["just one line"], col: 74, lnb: 43}

  """
  @spec new(binaries() | binary(), pos_integer(), pos_integer()) :: t()
  def new(lines, lnb \\ 1, col \\ 1)
  def new(lines, lnb, col) when is_list(lines), do: struct(__MODULE__, col: col, lines: lines, lnb: lnb)
  def new(line, lnb, col), do: new([line], lnb, col)

  @doc ~S"""
  Get next character (grapheme or symbol) from input

      iex(4)> input = new(~W[1 2])
      ...(4)> {g, i} = next_char(input)
      ...(4)> assert g == "1"
      ...(4)> assert i == new(["", "2"])
      ...(4)> {h, j} = next_char(i)
      ...(4)> assert h == :eol
      ...(4)> assert j == new("2", 2)

  And a shorter example, demonstrating the end of file behavior

      iex(5)> input = new("l")
      ...(5)> {g1, i1} = next_char(input)
      ...(5)> assert g1 == "l"
      ...(5)> assert i1 == new("")
      ...(5)> {g2, i2} = next_char(i1)
      ...(5)> assert g2 == :eol
      ...(5)> assert i2 == new([], 2)
      ...(5)> {g3, i3} = next_char(i2)
      ...(5)> assert g3 == :eof
      ...(5)> assert i3 == i2 # beware of endless loop after :eof

  UTF8, of course
      
      iex(6)> {g, _} = next_char(new("σπίτι"))
      ...(6)> g
      "σ"

  Demonstration of inner data during avancement

      
    
  """
  @spec next_char(t()) :: {token_t(), t()}
  def next_char(myself)
  def next_char(%__MODULE__{lines: []}=myself), do: {:eof, myself}
  def next_char(%__MODULE__{lines: [""|rest]}=myself), do: {:eol, %{myself|lines: rest, lnb: myself.lnb + 1}}
  def next_char(%__MODULE__{lines: [line|rest]}=myself) do
    <<char::utf8, line1::binary>> = line
    {List.to_string([char]), %{myself|lines: [line1|rest]}}
  end

  @spec position(t()) :: position_t()
  def position(%__MODULE__{col: col, lnb: lnb}), do: {col, lnb}
  
end
# SPDX-License-Identifier: Apache-2.0
