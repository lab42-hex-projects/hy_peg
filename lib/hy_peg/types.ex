defmodule HyPeg.Types do
  @moduledoc false

  defmacro __using__(_opts) do
    quote do
      # TODO: Check if we can make these types private!
      @type ast_t :: any()
      @type ast_list_t :: list(ast_t())

      @type either(success_t, error_t) :: {:ok, success_t} | {:error, error_t}
      # @type satisfier_result_t :: either(any(), binary())
      # @type satisfier_t :: (any() -> satisfier_result_t())

      @type input_t :: binary() | list(binary) | HyPeg.Input.t()

      @type maybe(t) :: nil | t
      @type binary? :: maybe(binary())
      @type binaries() :: list(binary())

      @type char_set_t :: list(binary()) | binary()

      # @type parser_function_t :: (HyPeg.Input.t(), HyPeg.Cache.t(), binary() -> result_t())

      @type result_t :: HyPeg.Failure.t() | HyPeg.Success.t()

      @type str_or_count_t :: binary() | non_neg_integer()
      # @type parse_function_t :: (HyPeg.Input.t(), HyPeg.Cache.t() -> result_t())
      # @type parser_t :: parse_function_t() | HyPeg.Parser.t()
      # @type predicate_t :: (HyPeg.Result.t() -> boolean())

      # @type result_t :: HyPeg.Success.t() | HyPeg.Failure.t()
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
