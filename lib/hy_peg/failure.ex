defmodule HyPeg.Failure do

  alias HyPeg.{Cache, Input}

  @moduledoc ~S"""
  A failure result
  """

  defstruct cache: %Cache{}, input: %Input{}, reason: ""

  @type t :: %__MODULE__{cache: Cache.t(), input: Input.t(), reason: binary()}


  @spec fail(binary(), Input.t(), Cache.t()) :: t
  def fail(reason, input, cache) do
    %__MODULE__{cache: cache, input: input, reason: reason}
  end
end

# SPDX-License-Identifier: Apache-2.0
