# defmodule HyPeg.Parsers do
#   use HyPeg.Types
#   alias HyPeg.Input

#   defstruct name: nil, fun: nil


#   @typep parse_result_t :: {:ok, any(), Input.t} | {:error, binary()}

#   @typep parser_fun_t :: (Input.t -> parse_result_t())

#   @type t :: %__MODULE__(name: binary?(), fun: maybe(parser_fun_t())}
#   @moduledoc ~S"""
#   Implements all basic parsers
#   """

#   @spec char_parser(
#   def char_parser(set \\ nil, name \\ nil)

# end
# SPDX-License-Identifier: Apache-2.0
