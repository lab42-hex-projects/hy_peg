defmodule HyPeg do
  @moduledoc """
  Documentation for `HyPeg`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> HyPeg.hello()
      :world

  """
  def hello do
    :world
  end
end
