defmodule HyPeg.MixProject do
  use Mix.Project

  @project "HyPeg"
  @version "0.0.1"

  @url "https://gitlab.com/lab42-hex-projects/hy_peg"

  def project do
    [
      aliases: [docs: &build_docs/1],
      description: "Hybrid parser expression library (line oriented or char oriented, supporting rgxen)",
      elixirc_paths: elixirc_paths(Mix.env()),
      maintainers: [
        "Robert Dober <robert.dober@gmail.com>",
      ],
      package: package(),
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      test_coverage: [tool: ExCoveralls],
      start_permanent: Mix.env() == :prod,
      version: @version,
      app: :hy_peg,
      version: @version,
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  @prerequisites """
  run `mix escript.install hex ex_doc` and adjust `PATH` accordingly
  """
  defp build_docs(_) do
    Mix.Task.run("compile")
    ex_doc = Path.join(Mix.path_for(:escripts), "ex_doc")
    Mix.shell().info("Using escript: #{ex_doc} to build the docs")

    unless File.exists?(ex_doc) do
      raise "cannot build docs because escript for ex_doc is not installed, make sure to \n#{@prerequisites}"
    end

    args = [@project, @version, Mix.Project.compile_path()]
    opts = ~w[--main #{@project} --source-ref v#{@version} --source-url #{@url}]

    Mix.shell().info("Running: #{ex_doc} #{inspect(args ++ opts)}")
    System.cmd(ex_doc, args ++ opts)
    Mix.shell().info("Docs built successfully")
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:dialyxir, "~> 1.4.3", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.18.0", only: [:test]},
      {:extractly, "~> 0.5.4", only: [:dev]},
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp package do
    [
      files: [
        "lib",
        "mix.exs",
        "README.md"
      ],
      maintainers: [
        "Robert Dober <robert.dober@gmail.com>",
      ],
      licenses: [
        "Apache-2.0"
      ],
      links: %{
        "Gitlab" => @url
      }
    ]
  end
  # Run "mix help deps" to learn about dependencies.
end
# SPDX-License-Identifier: Apache-2.0
